#!/bin/bash

# Chart URL required
HELM_CHART_HOME="https://external-secrets.github.io/kubernetes-external-secrets/"
NAMESPACE="external-secrets"
CHART_NAME="external-secrets/kubernetes-external-secrets"
EXTERNAL_SECRETS_GSA="external-secrets-k8s"
# Change me IF needed to point at a different root repo
TARGET_REPO_BASEDIR="../config"
EXTERNAL_SECRETS_GSA_KEY="gsa-external-secrets-key"
UPGRADE="false"

echo "Starting..."

clean

if [ ! -d "${TARGET_REPO_BASEDIR}/namespaces/${NAMESPACE}" ]; then
	echo "Installing for the first time..."
else
	echo "Upgrading existing installation..."
	UPGRADE="true"
fi

source ./helper/add-helm-chart-acm.sh

# Required TEMPLATE_RESULT
RELEASE_NAME=$1

if [ -z "${RELEASE_NAME}" ]; then
	echo "Please call with the desired name of the release  ./add-external-secrets.sh release-0.1"
	exit 1
fi

gcloud services enable \
    cloudresourcemanager.googleapis.com \
    secretmanager.googleapis.com


function clean() {
  rm -rf ${OUT_CLUSTER_SCOPED} ${OUT_NAMESPACE_SCOPED} ${TEMPLATE_RESULT} *.yaml 2>/dev/null
}

# NOTE: This implementation is NOT based on WorkloadIdentity. IF it is possible to use WI, please do!
# Since this is NOT WorkloadIdentity based, GCP service calls need to use a GSA's keys and APPLICATION_DEFAULT_CREDENTIALS.
# Need to create a GSA specifically for secrets reading, create a key (if not exists already), then add to k8s (if not exists)
# Chart picks up the secret and mounts for the deployment

function create_enable_service_account() {
    local SA_NAME="$1"
    local PROJECT="${2-$PROJECT_ID}"
    local DESCRIPTION="${3-Service account for External Secrets}"
    local CURR_SA_NAME=$(gcloud iam service-accounts describe ${SA_NAME}@${PROJECT}.iam.gserviceaccount.com --format="value(name)" 2> /dev/null)
    if [ -z "$CURR_SA_NAME" ]; then
        gcloud iam service-accounts create ${SA_NAME} --display-name "${DESCRIPTION}"

    else
        gcloud iam service-accounts enable "${SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"
    fi
}

function disable_service_account() {
    local SA_NAME="$1"
    local PROJECT="${2-$PROJECT_ID}"
    local CURR_SA_NAME=$(gcloud iam service-accounts describe ${SA_NAME}@${PROJECT}.iam.gserviceaccount.com --format="value(name)" 2> /dev/null)
    if [ ! -z "$CURR_SA_NAME" ]; then
        gcloud iam service-accounts disable "${SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" -q
    fi
}

# create GSA if not exists
create_enable_service_account ${EXTERNAL_SECRETS_GSA}
# Store JSON key as secret
store_service_account_key "${EXTERNAL_SECRETS_GSA_KEY}" "${EXTERNAL_SECRETS_GSA}"

# Give secrets manager viewer capabilities
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${EXTERNAL_SECRETS_GSA}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/secretmanager.viewer"

##### Setup files for root-repo

setup_helm ${HELM_CHART_HOME}
render_template ${CHART_NAME} ${RELEASE_NAME} ${NAMESPACE}
split_template ${NAMESPACE}

## Start copying to the repo folder
mkdir -p ${TARGET_REPO_BASEDIR}/namespaces/${NAMESPACE}

if [ "${UPGRADE}" == "false" ]; then
	# Create a new namespace
	cat > namespace.yaml <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: ${NAMESPACE}
  labels:
    created-by: root-configuration
    generated: true
EOF
cp namespace.yaml "${TARGET_REPO_BASEDIR}/namespaces/${NAMESPACE}/"
fi

HAS_SECRET=$(kubectl get secret -n ${NAMESPACE} gcp-creds -o jsonpath='{.metadata.name}' 2> /dev/null)
if [ -z "${HAS_SECRET}" ]; then
	# Create the k8s secret using the GCP Secrets Manager contents
	CREDS_FILE_CONTENTS=$(gcloud secrets -q versions access latest --secret="${EXTERNAL_SECRETS_GSA_KEY}")
	kubectl create secret generic gcp-creds --namespace "${NAMESPACE}" --from-literal=gcp-creds.json="${CREDS_FILE_CONTENTS}" --dry-run=client -o yaml > kubectl-secret-for-clusters.yaml
	echo -e "\n\n===================================\n"
	echo "For Non-WorkloadIdentity configuration, the 'gcp-creds' secret needs to be added to all clusters using External Secrets"
	echo -e "\n=====================================\n\n"
fi

# Copy to the repo folder
cp ${OUT_CLUSTER_SCOPED}/cluster/*.yaml ${TARGET_REPO_BASEDIR}/cluster/
cp ${OUT_NAMESPACE_SCOPED}/${NAMESPACE}/*.yaml ${TARGET_REPO_BASEDIR}/namespaces/${NAMESPACE}/